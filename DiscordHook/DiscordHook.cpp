#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
DWORD ScanPattern(VOID* Address, ULONG Length, BYTE* Signature, CHAR* Mask, ULONG Offset)
{
	for (int i = 0; i <= (Length - strlen(Mask)) + 1; i++)
	{
		if ((*(PBYTE)((PBYTE)Address + i) == Signature[0] && Mask[0] == 'x') || (Mask[0] == '?'))
		{
			for (int x = 0;; x++)
			{
				if (Mask[x] == 'x')
				{
					if ((*(PBYTE)((PBYTE)Address + i + x) == Signature[x]))
						continue;
					else
						break;
				}
				else if (Mask[x] == 0x00)
				{
					return (ULONG)(*(PBYTE)((PBYTE)Address + i + Offset));
				}
			}
		}
	}
	return 0;
}

LPVOID Address = 0;

__declspec(naked) PULONG __cdecl SSL_Write(VOID * SSL_Ctx, CHAR * buf, INT len)
{
	__asm
	{
		push ebp
		mov ebp, esp
		push esi
		mov esi, dword ptr[ebp + 8]
		mov eax, Address
		add eax, 7
		jmp eax
	}
}


typedef struct _HTTP2
{
	unsigned char Length[3];
	unsigned char Type;
	unsigned char Flags;
	unsigned int StreamIdentifier;
}HTTP2;
#pragma warning(disable:4996)
PULONG __cdecl SSL_WriteHook(VOID * SSL_Ctx, CHAR * buf, INT len)
{

	BYTE Signature[] = { 0x63 , 0x6f , 0x6e , 0x74 , 0x65 , 0x6e , 0x74 };

	if (len > 64)
	{
		if (!memcmp((void*)((unsigned long)(buf)+11), Signature, sizeof(Signature)))
		{

			char * newbuf = (char*)malloc(len);
			char ID[1024] = { 0 };
			int i = 0;
			int payloadlength = 0x33333333;

			int a = 0; // buf[2] - 55;

			*(unsigned char*)(&a) = buf[2];
			*(unsigned char*)(((unsigned long)(&a) + 1)) = buf[1];
			*(unsigned char*)(((unsigned long)(&a) + 2)) = buf[0];

			a -= 55;

			memcpy(newbuf, buf, len);

			sprintf(ID, "Number: %d", a);
			OutputDebugStringA(ID);


			for (; i < a; i++)
			{
				newbuf[21 + i] ^= 1;
			}


			a = (int)SSL_Write(SSL_Ctx, newbuf, len);
			free(newbuf);

			return (PULONG)a;
		}
	}

	return SSL_Write(SSL_Ctx, buf, len);
}

ULONG CALLBACK Thread(LPVOID Context)
{
	ULONG OldProtection = 0;

	Address = /*(LPVOID)0x030ED260;*/  GetModuleHandleW(0) + 0x0288D260;

	if (!VirtualProtect(Address, 1, PAGE_EXECUTE_READWRITE, &OldProtection))
	{
		char Error[10] = { 0 };
		sprintf(Error, "%2X", GetLastError());

		MessageBoxA(0, Error, 0, 0);
		return 0;
	}

	memset(Address, 0x90, 7);

	*(PBYTE)Address = 0xE9;
	*(PULONG)((PBYTE)Address + 1) = (ULONG)SSL_WriteHook - (ULONG)Address - 5;

	return 1;
}

BOOL APIENTRY DllMain(HMODULE Instance, ULONG Reason, VOID* Reserved)
{
	static HANDLE ThreadHandle = NULL;
	DisableThreadLibraryCalls(Instance);
	switch (Reason)
	{
	case DLL_PROCESS_ATTACH:
	{

		ThreadHandle = CreateThread(NULL, 0, Thread, Instance, 0, NULL);
		return 1;
	}
	break;
	case DLL_PROCESS_DETACH:
	{

	}
	break;
	}
	return 0;
}